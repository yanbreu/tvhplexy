FROM golang:alpine AS builder
WORKDIR /go/src/gitlab.com/yanbreu/tvhplexy
COPY . /go/src/gitlab.com/yanbreu/tvhplexy
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o tvhplexy-bin .

FROM alpine:latest
WORKDIR /data/app
COPY --from=builder /go/src/gitlab.com/yanbreu/tvhplexy/tvhplexy-bin /usr/bin/tvhplexy
ENTRYPOINT ["tvhplexy"]