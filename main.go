package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"html"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

var tvhURL = "http://plex:Plex!1234@192.168.178.4:9981"
var uuid string
var port string

func init() {
	flag.StringVar(&tvhURL, "tvhurl", "", "Tvheadend URL (e.g. http://localhost:9981)")
	flag.StringVar(&port, "port", "5004", "TvhPlexy port")
	flag.StringVar(&uuid, "uuid", "11b93b6358fc1dd2307e350432a2c3bb", "UUID for TvhPlexy Proxy")
	flag.Parse()

	if os.Getenv("TVHPLEXY_TVHURL") != "" {
		tvhURL = os.Getenv("TVHPLEXY_TVHURL")
	}
	if os.Getenv("TVHPLEXY_PORT") != "" {
		port = os.Getenv("TVHPLEXY_PORT")
	}
	if os.Getenv("TVHPLEXY_UUID") != "" {
		uuid = os.Getenv("TVHPLEXY_UUID")
	}
}

type DiscoverDataModel struct {
	FriendlyName    string `json:"FriendlyName"`
	Manufacturer    string `json:"Manufacturer"`
	ModelNumber     string `json:"ModelNumber"`
	FirmwareName    string `json:"FirmwareName"`
	TunerCount      int    `json:"TunerCount"`
	FirmwareVersion string `json:"FirmwareVersion"`
	DeviceID        string `json:"DeviceID"`
	DeviceAuth      string `json:"DeviceAuth"`
	BaseURL         string `json:"BaseURL"`
	LineupURL       string `json:"LineupURL"`
}

type Channel struct {
	GuideNumber string `json:"GuideNumber"`
	GuideName   string `json:"GuideName"`
	URL         string `json:"URL"`
}

type LineupStatus struct {
	ScanInProgress int      `json:"ScanInProgress"`
	ScanPossible   int      `json:"ScanPossible"`
	Source         string   `json:"Source"`
	SourceList     []string `json:"SourceList"`
}

type TvhGridChannel struct {
	UUID    string `json:"uuid"`
	Enabled bool   `json:"enabled"`
	Name    string `json:"name"`
	Number  int    `json:"number"`
}

type TvhGridResponse struct {
	Entries []TvhGridChannel `json:"entries"`
	Total   int              `json:"total"`
}

func main() {
	tvhURL = strings.Trim(tvhURL, "/")
	fmt.Println("Tvheadend URL: " + tvhURL)
	fmt.Println("TvhPlexy Port: " + port)
	fmt.Println("TvhPlexy UUID: " + uuid)

	var discoverData = DiscoverDataModel{
		FriendlyName:    "TvhPlexy",
		Manufacturer:    "hd",
		ModelNumber:     "HDTC-2US",
		FirmwareName:    "hdhomeruntc_atsc",
		TunerCount:      10,
		FirmwareVersion: "20150826",
		DeviceID:        uuid,
		DeviceAuth:      "test1234",
		BaseURL:         tvhURL,
		LineupURL:       tvhURL + "lineup.json",
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RemoteAddr, r.URL)
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
	})

	http.HandleFunc("/discover.json", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RemoteAddr, r.URL)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		json.NewEncoder(w).Encode(discoverData)
	})
	http.HandleFunc("/device.xml", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RemoteAddr, r.URL)
		w.Header().Set("Content-Type", "application/xml; charset=UTF-8")

		tmpl, err := template.New("test").Parse(`
<root xmlns="urn:schemas-upnp-org:device-1-0">
    <specVersion>
        <major>1</major>
        <minor>0</minor>
    </specVersion>
    <URLBase>{{ .BaseURL }}</URLBase>
    <device>
        <deviceType>urn:schemas-upnp-org:device:MediaServer:1</deviceType>
        <friendlyName>{{ .FriendlyName }}</friendlyName>
        <manufacturer>{{ .Manufacturer }}</manufacturer>
        <modelName>{{ .ModelNumber }}</modelName>
        <modelNumber>{{ .ModelNumber }}</modelNumber>
        <serialNumber></serialNumber>
        <UDN>uuid:{{ .DeviceID }}</UDN>
    </device>
</root>
		`)
		if err != nil {
			log.Fatalln(err.Error())
			return
		}

		tmpl.Execute(w, discoverData)
	})
	http.HandleFunc("/lineup.json", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RemoteAddr, r.URL)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		resp, err := http.Get(tvhURL + "/api/channel/grid?start=0&limit=999999")
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
		var tvhGridResponseData TvhGridResponse
		err = json.NewDecoder(resp.Body).Decode(&tvhGridResponseData)
		if err != nil {
			log.Fatalln(err.Error())
			return
		}
		channels := make([]Channel, tvhGridResponseData.Total)
		for i, channel := range tvhGridResponseData.Entries {
			if !channel.Enabled {
				continue
			}
			channels[i] = Channel{
				GuideNumber: strconv.Itoa(channel.Number),
				GuideName:   channel.Name,
				URL:         fmt.Sprintf("%s/stream/channel/%s?profile=%s&weight=%d", discoverData.BaseURL, channel.UUID, "pass", 100),
			}
		}
		json.NewEncoder(w).Encode(channels)
	})

	http.HandleFunc("/lineup_status.json", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.RemoteAddr, r.URL)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		json.NewEncoder(w).Encode(LineupStatus{
			ScanInProgress: 0,
			ScanPossible:   1,
			Source:         "Cable",
			SourceList:     []string{"Cable"},
		})
	})

	log.Fatal(http.ListenAndServe(":"+port, nil))
}
